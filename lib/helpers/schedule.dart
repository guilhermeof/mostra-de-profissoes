import 'package:cloud_firestore/cloud_firestore.dart';

class Schedule {
  final String dia;
  final String mes;
  final List<Sessions> sessions;

  Schedule({this.dia, this.mes, this.sessions});
}

class Sessions {
  final String nome;
  final String local;
  final String descricao;
  final Object palestrantes;
  final String inico;
  final String termino;
  final String tag;
  final String id;

  Sessions(
      {this.nome,
      this.local,
      this.descricao,
      this.palestrantes,
      this.inico,
      this.termino,
      this.tag,
      this.id});
}

_parseFirebase(QuerySnapshot snapshot) {
  var days = snapshot.documents;

  List<Schedule> schedules = new List<Schedule>();

  for (var day in days){
    List<Sessions> sessions = new List<Sessions>();

    Stream<QuerySnapshot> sessoes = day.reference.collection('sessoes').getDocuments().asStream();

sessoes.listen((data){
      data.documents.forEach((item){
      sessions.add(new Sessions(
          nome: item['nome'],
          local: item['local'],
          descricao: item['descricao'],
          palestrantes: item['palestrantes'],
          inico: item['inicio'],
          termino: item['termino'],
          tag: item['tag'],
          id: item['id']
      ));
     });
    });

    sessions.sort((a,b) => a.inico.compareTo(b.inico));

    schedules.add(new Schedule(dia: day['dia'], mes: day['mes'], sessions: sessions));
  }

  return schedules;
}

Future<QuerySnapshot> _loadSchedules() async {
  Firestore firestore;
  firestore = Firestore.instance;
  QuerySnapshot querySnapshot =
  await firestore.collection('schedule').getDocuments();

  return querySnapshot;
}

void allSchedules() async {
  QuerySnapshot data = await _loadSchedules(); 

  scheduleData = await _parseFirebase(data);
}

List<Schedule> scheduleData = [];