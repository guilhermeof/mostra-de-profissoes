import 'package:app_event/helpers/color_hex.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        child: Stack(
          children: <Widget>[
            Container(
               decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                Color(getColorHex('0CE7F0')),
                Color(getColorHex('0098D9')),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.0, 1.0],
            )),
            ),
            SingleChildScrollView(
                          child: Container(
                margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top + 5),
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 3,),
                    _conteudo("Realização", 'assets/images/logo_prog.png', 100, 300),
                    SizedBox(height: 20,),
                    _conteudo("Patrocínio", 'assets/images/logo_fapead.png', 100, 300),
                    SizedBox(height: 20,),
                    _conteudo("Apoio", 'assets/images/logo_uemanet.png', 100 , 300),
                    SizedBox(height: 15,),
                    _conteudoApoio("", 'assets/images/logo_ensinar.png', 90 , 300),
                  ],
                ),
              ),
            ),
            _backButton(context)
          ],
        ),
      ),
    );
  }

  Container _backButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: BackButton(color: Colors.white),
    );
  }

  _conteudo(String titulo, String path, double height, double width) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            titulo,
            style: TextStyle(fontSize: 30, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: height,
            width: width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(path))),
          )
        ],
      ),
    );
  }

  _conteudoApoio(String titulo, String path, double height, double width) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: height,
            width: width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(path))),
          )
        ],
      ),
    );
  }
}
