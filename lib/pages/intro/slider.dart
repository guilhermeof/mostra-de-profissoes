import 'package:app_event/helpers/color_hex.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';

class MySlider extends StatefulWidget {
// MySlider({Key key}) : super(key: key);

  @override
  MySliderState createState() => MySliderState();
}

// Custom config
class MySliderState extends State<MySlider> {
  List<Slide> slides = List();

  Future<List<Slide>> _createSlides(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
            top: size.height * 0.09, bottom: size.height * 0.01),
        title: "Bem Vindo ao APP UEMA Orienta",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontFamily: 'CaveatBrush',
          fontSize: 50,
          color: Colors.white,
          shadows: <Shadow>[
            Shadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.02,
            left: size.width * 0.05,
            right: size.width * 0.05),
        description:
            "O APP UEMA Orienta é uma forma rápida e fácil de você acessar as informações essenciais deste evento.",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
        pathImage: "assets/images/logo.png",
        heightImage: size.height * 0.35,
        colorBegin: Color(getColorHex('0CE7F0')),
        colorEnd: Color(getColorHex('0098D9')),
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomCenter,
      ),
    );

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
            top: size.height * 0.09, bottom: size.height * 0.07),
        title: "Programação",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontFamily: 'CaveatBrush',
          fontSize: 50,
          color: Colors.white,
          shadows: <Shadow>[
            Shadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.05,
            left: size.width * 0.06,
            right: size.width * 0.06),
        description:
            "Você pode visualizar a programação do evento, saiba tudo sobre as atividades que deseja acompanhar.",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontFamily: 'Raleway',
        ),
        pathImage: "assets/images/1.png",
        heightImage: size.height * 0.35,
        colorBegin: Color(getColorHex('0CE7F0')),
        colorEnd: Color(getColorHex('0098D9')),
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomCenter,
      ),
    );

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
            top: size.height * 0.09, bottom: size.height * 0.07),
        title: "Catálogo dos Cursos",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontFamily: 'CaveatBrush',
          fontSize: 50,
          color: Colors.white,
          shadows: <Shadow>[
            Shadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.05,
            left: size.width * 0.03,
            right: size.width * 0.03),
        description:
            "Conheça todos nossos cursos, informe-se aasobre as cargas horárias e áreas de atuação profissional.",
        styleDescription: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontFamily: 'Raleway',
        ),
        pathImage: "assets/images/2.png",
        heightImage: size.height * 0.35,
        colorBegin: Color(getColorHex('0CE7F0')),
        colorEnd: Color(getColorHex('0098D9')),
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomCenter,
      ),
    );

    slides.add(
      Slide(
        marginTitle: EdgeInsets.only(
            top: size.height * 0.09, bottom: size.height * 0.05),
        title: "Tenha um bom Evento!",
        maxLineTitle: 2,
        styleTitle: TextStyle(
          fontFamily: 'CaveatBrush',
          fontSize: 50,
          color: Colors.white,
          shadows: <Shadow>[
            Shadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: Offset(0.0, 5.0),
            ),
          ],
        ),
        marginDescription: EdgeInsets.only(
            top: size.height * 0.05,
            left: size.width * 0.04,
            right: size.width * 0.04),
        description:
            "UEMA, em nome de toda a equipe da organização, deseja um ótimo evento!",
        styleDescription: TextStyle(
            color: Colors.white, fontSize: 20.0, fontFamily: 'Raleway'),
        pathImage: "assets/images/3.png",
        heightImage: size.height * 0.32,
        colorBegin: Color(getColorHex('0CE7F0')),
        colorEnd: Color(getColorHex('0098D9')),
        directionColorBegin: Alignment.topCenter,
        directionColorEnd: Alignment.bottomCenter,
      ),
    );

    return Future.value(slides);
  }

  void onDonePress() {
    // Do what you want
    Navigator.pushReplacementNamed(context, '/home');
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      // color: Color(getColorHex('275273')),
      color: Colors.white,
      size: 35.0,
    );
  }

  Widget renderDoneBtn() {
    return Icon(Icons.done, color: Colors.white);
  }

  Widget renderSkipBtn() {
    // return Icon(
    //   Icons.skip_next,
    //   // color: Color(0xffD02090),
    //   color: Colors.blue[900],
    // );

    return Text(
      'Pular',
      style: TextStyle(
        // color: Color(getColorHex('275273')),
        color: Colors.white,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Slide>>(
      future: _createSlides(context),
      builder: (BuildContext context, AsyncSnapshot<List<Slide>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return CircularProgressIndicator();
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            return IntroSlider(
              // List slides
              slides: this.slides,

              // Skip button
              renderSkipBtn: this.renderSkipBtn(),
              // colorSkipBtn: Color(0x33000000),
              // highlightColorSkipBtn: Color(0xff000000),

              // Next button
              renderNextBtn: this.renderNextBtn(),

              // Done button
              renderDoneBtn: this.renderDoneBtn(),
              onDonePress: this.onDonePress,
              // colorDoneBtn: Color(0x33000000),
              // highlightColorDoneBtn: Color(0xff000000),

              // Dot indicator
              // colorDot: Color(getColorHex('6ba2cc')),
              colorDot: Colors.white,
              colorActiveDot: Color(getColorHex('F58634')),
              // colorDot: Color(0x33D02090),
              // colorActiveDot: Color(0xffD02090),
              sizeDot: 13.0,

              // Show or hide status bar
              shouldHideStatusBar: false,
            );
        }
        return null; // unreachable
      },
    );
  }
}

// import 'package:app_event/helpers/color_hex.dart';
// import 'package:flutter/material.dart';
// import 'package:intro_slider/intro_slider.dart';

// class MySlider extends StatefulWidget {
// // MySlider({Key key}) : super(key: key);

//   @override
//   MySliderState createState() => MySliderState();
// }

// // Custom config
// class MySliderState extends State<MySlider> {
//   List<Slide> slides = List();

//   @override
//   void initState() {
//     super.initState();

//     slides.add(
//       Slide(
//         // heightImage: MediaQuery.of(context).size.height * 0.1,
//         // marginTitle:
//         //     EdgeInsets.only(top: MediaQuery.of(context).size.height - 500),
//         title: "Bem vindo ao APP UEMA Orienta",
//         maxLineTitle: 2,
//         styleTitle: TextStyle(
//           fontFamily: 'CaveatBrush',
//           fontSize: 50,
//           // fontWeight: FontWeight.bold,
//           color: Color(getColorHex('275273')),
//         ),
//         marginDescription: EdgeInsets.only(top: 40, left: 15, right: 15),
//         description:
//             "O APP UEMA Orienta é uma forma rápida e fácil de você acessar as informações essenciais deste evento.",
//         styleDescription: TextStyle(
//           color: Color(getColorHex('275273')),
//           fontSize: 20,
//         ),
//         pathImage: "assets/images/logo.png",
//         colorBegin: Color(getColorHex('FFA500')),
//         colorEnd: Color(getColorHex('7FFFD4')),
//         directionColorBegin: Alignment.topCenter,
//         directionColorEnd: Alignment.bottomCenter,
//       ),
//     );

//     slides.add(
//       Slide(
//         // marginTitle: EdgeInsets.only(top: 50, left: 15, right: 15, bottom: 40),
//         title: "Programação",
//         maxLineTitle: 2,
//         styleTitle: TextStyle(
//           fontFamily: 'CaveatBrush',
//           fontSize: 50,
//           // fontWeight: FontWeight.bold,
//           color: Color(getColorHex('275273')),
//         ),
//         // marginDescription: EdgeInsets.only(top: 40, left: 15, right: 15),
//         description:
//             "Você pode visualizar a programação do evento, saiba tudo sobre as atividades que deseja acompanhar.",
//         styleDescription: TextStyle(
//           color: Color(getColorHex('275273')),
//           fontSize: 20.0,
//           fontFamily: 'Raleway',
//         ),
//         pathImage: "assets/images/1.png",
//         colorBegin: Color(getColorHex('FFA500')),
//         colorEnd: Color(getColorHex('7FFFD4')),
//         directionColorBegin: Alignment.topCenter,
//         directionColorEnd: Alignment.bottomCenter,
//       ),
//     );

//     slides.add(
//       Slide(
//         // marginTitle: EdgeInsets.only(top: 50, left: 15, right: 15, bottom: 40),
//         title: "Catálogo dos Cursos",
//         maxLineTitle: 2,
//         styleTitle: TextStyle(
//           fontFamily: 'CaveatBrush',
//           fontSize: 50,
//           // fontWeight: FontWeight.bold,
//           color: Color(getColorHex('275273')),
//         ),
//         // marginDescription: EdgeInsets.only(top: 40, left: 15, right: 15),
//         description:
//             "Conheça todos nossos cursos, informe-se sobre as cargas horárias e áreas de atuação profissional.",
//         styleDescription: TextStyle(
//           color: Color(getColorHex('275273')),
//           fontSize: 20.0,
//           fontFamily: 'Raleway',
//         ),
//         pathImage: "assets/images/2.png",
//         colorBegin: Color(getColorHex('FFA500')),
//         colorEnd: Color(getColorHex('7FFFD4')),
//         directionColorBegin: Alignment.topCenter,
//         directionColorEnd: Alignment.bottomCenter,
//         // backgroundImageFit: BoxFit.fill,
//       ),
//     );

//     slides.add(
//       Slide(
//         // marginTitle: EdgeInsets.only(top: 50, left: 15, right: 15, bottom: 40),
//         title: "Tenha um bom evento!",
//         maxLineTitle: 2,
//         styleTitle: TextStyle(
//           fontFamily: 'CaveatBrush',
//           fontSize: 50,
//           // fontWeight: FontWeight.bold,
//           color: Color(getColorHex('275273')),
//         ),
//         // marginDescription: EdgeInsets.only(top: 40, left: 15, right: 15),
//         description:
//             "UEMA, em nome de toda a equipe da organização, deseja um ótimo evento!",
//         styleDescription: TextStyle(
//             color: Color(getColorHex('275273')),
//             fontSize: 20.0,
//             fontFamily: 'Raleway'),
//         pathImage: "assets/images/3.png",
//         colorBegin: Color(getColorHex('FFA500')),
//         colorEnd: Color(getColorHex('7FFFD4')),
//         directionColorBegin: Alignment.topCenter,
//         directionColorEnd: Alignment.bottomCenter,
//       ),
//     );
//   }

//   void onDonePress() {
//     // Do what you want
//     Navigator.pushReplacementNamed(context, '/home');
//   }

//   Widget renderNextBtn() {
//     return Icon(
//       Icons.navigate_next,
//       color: Color(getColorHex('275273')),
//       size: 35.0,
//     );
//   }

//   Widget renderDoneBtn() {
//     return Icon(
//       Icons.done,
//       color: Color(getColorHex('275273')),
//     );
//   }

//   Widget renderSkipBtn() {
//     // return Icon(
//     //   Icons.skip_next,
//     //   // color: Color(0xffD02090),
//     //   color: Colors.blue[900],
//     // );

//     return Text(
//       'Pular',
//       style: TextStyle(
//         color: Color(getColorHex('275273')),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return IntroSlider(
//       // List slides
//       slides: this.slides,

//       // Skip button
//       renderSkipBtn: this.renderSkipBtn(),
//       // colorSkipBtn: Color(0x33000000),
//       // highlightColorSkipBtn: Color(0xff000000),

//       // Next button
//       renderNextBtn: this.renderNextBtn(),

//       // Done button
//       renderDoneBtn: this.renderDoneBtn(),
//       onDonePress: this.onDonePress,
//       // colorDoneBtn: Color(0x33000000),
//       // highlightColorDoneBtn: Color(0xff000000),

//       // Dot indicator
//       // colorDot: Color(getColorHex('6ba2cc')),
//       colorDot: Color(getColorHex('0098DA')),
//       colorActiveDot: Color(getColorHex('275273')),
//       // colorDot: Color(0x33D02090),
//       // colorActiveDot: Color(0xffD02090),
//       sizeDot: 13.0,

//       // Show or hide status bar
//       shouldHideStatusBar: false,
//     );
//   }
// }
