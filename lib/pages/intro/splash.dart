import 'package:app_event/helpers/schedule.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SplashPage extends StatefulWidget {
  bool prefs;
  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {
  startSplashScreenTimer(String page) async {
    var _duration = Duration(seconds: 5);
    return Timer(
        _duration, () => Navigator.pushReplacementNamed(context, page));
  }

  @override
  void initState() {
    super.initState();
    allSchedules();
    _prefs();
  }

  _prefs() async {
    SharedPreferences tutorial = await SharedPreferences.getInstance();
    widget.prefs = tutorial.getBool('tutorial');

    if (widget.prefs == null) {
      _savePrefs(true);
      startSplashScreenTimer('/tutorial');
    } else {
      _savePrefs(false);
      startSplashScreenTimer('/home');
    }
  }

  _savePrefs(bool tutorial) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("tutorial", tutorial);
  }

//LOGO

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: size.height * 0.36),
            ),
            Image.asset('assets/images/logo.png'),
            // SizedBox(height: 20,),
            // Container(height: 50,),
            // CircularProgressIndicator()
          ],
        ),
      ),
    );
  }
}
