import 'package:app_event/helpers/color_hex.dart';
import 'package:app_event/pages/areas/areas_page.dart';
import 'package:app_event/ui/common/separator.dart';
import 'package:app_event/ui/styles/text_style.dart';
import 'package:flutter/material.dart';

class VestibularPage extends StatefulWidget {
  @override
  _VestibularPageState createState() => _VestibularPageState();
}

class _VestibularPageState extends State<VestibularPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        height: double.infinity,
        child: Stack(
          children: <Widget>[
            Container(
              height: 180,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(getColorHex('0CE7F0')),
                    Color(getColorHex('0098D9')),
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(0.0, 1.0),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Vestibular".toUpperCase(), style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                      ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 140),
              height: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadiusDirectional.only(
                      topStart: Radius.circular(25),
                      topEnd: Radius.circular(25))),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: <Widget>[
                              _content('Data do próximo vestibular:', 'Vestibular 2020'),
                              _content('1ª etapa:', '20 de outubro de 2019'),
                              _content('2ª etapa:', '24 de novembro de 2019'),
                              _contentOnTap('Cursos ofertados:', ''),
                              _contentWarning('Período de isenção da taxa:', '06 a 31 de maio de 2019'),
                              _content('Período de Inscrição:', '15 de julho a 09 de agosto de 2019'),
                              _content('Obras Literárias:', 'Libertinagem (Manuel Bandeira)'),
                              _contentObras('','Verão no Aquário (Lygia Fagundes Telles)'),
                              _contentObras('','Memórias de um Sargento de Milícias (Manoel Antonio Almeida)'),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            _backButton(context)
          ],
        ),
      ),
    );
  }

  Container _backButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        tooltip: MaterialLocalizations.of(context).closeButtonTooltip,
        onPressed: () {
          Navigator.maybePop(context);
        },
      ),
    );
  }

  _content(String title, String content) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Separator(),
                  Text(
                    title,
                    style: Style.titleTextStyle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Text(
                      content,
                      style: Style.commonTextStyle,
                      // style: TextStyle(height: 1.5),
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(height: 7),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _contentOnTap(String title, String content) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Separator(),
                  Text(
                    title,
                    style: Style.titleTextStyle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 10),
                  Container(
                      alignment: Alignment.center,
                      child: RaisedButton(
                          child: Text(
                              "Clique aqui e confira nossa lista de cursos"),
                          textColor: Colors.white,
                          color: Color(getColorHex('F58634')),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) {
                                return AreasPage();
                              }),
                            );
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)))),
                  SizedBox(height: 5),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

_contentWarning(String title, String content) {
  return Container(
    child: Row(
      children: <Widget>[
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Separator(),
                Text(
                  title,
                  style: Style.titleTextStyle,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.warning, color: Colors.orange),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        content,
                        style: Style.commonTextStyle,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                ),
                SizedBox(height: 7),
              ],
            ),
          ),
        )
      ],
    ),
  );
}

_contentObras(String title, String content) {
  return Container(
    child: Row(
      children: <Widget>[
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                    content,
                    style: Style.commonTextStyle,
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
        )
      ],
    ),
  );
}
