import 'package:app_event/helpers/color_hex.dart';
import 'package:app_event/pages/about/about.dart';
import 'package:app_event/pages/areas/areas_page.dart';
import 'package:app_event/pages/courses/courses.dart';
import 'package:app_event/pages/schedule/schedule_page.dart';
import 'package:app_event/pages/vestibular/vestibular_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageController controller = PageController(viewportFraction: 0.8);

  List menuData = [
    {
      'title': 'Programação',
      'descricao': 'Fique por dentro do que irá acontecer.',
      'image': 'assets/images/home/programacao.png',
      'page': SchedulePage()
    },
    {
      'title': 'Cursos UEMA',
      'descricao': 'Confira todos os nossos cursos',
      'image': 'assets/images/home/cursos.png',
      'page': AreasPage()
    },
    // {
    //   'title': 'Cursos UEMA',
    //   'descricao': 'Confira todos os nossos cursos',
    //   'image': 'assets/images/home/cursos.png',
    //   'page': CoursesList()
    // },
    {
      'title': 'Vestibular',
      'descricao': 'Acompanhe o nosso vestibular.',
      'image': 'assets/images/home/vestibular.png',
      'page': VestibularPage(
      )
    },
    {
      'title': 'Realizadores',
      'descricao': 'Conheça nossos apoiadores',
      'image': 'assets/images/home/about.png',
      'page': AboutPage()
    },
    // {
    //   'title': 'Local',
    //   'descricao': 'Conheça o local do evento.',
    //   'image': 'assets/images/home/local.png',
    //   'page': MapPage()
    // },
    // {
    //   'title': 'Teste Vocacional',
    //   'descricao': '',
    //   'image': 'assets/images/home/vocacional.png',
    //   'page': VacationalTest()
    // },
  ];

  int currentPage = 0;

  @override
  void initState() {
    controller.addListener(() {
      int next = controller.page.round();
      if (currentPage != next) {
        setState(() {
          currentPage = next;
        });
      }
    });
    super.initState();
  }

  AnimatedContainer _buildStoryPage(Map data, bool active) {
    // Animated properties
    final double blur = active ? 4 : 0;
    final double offset = active ? 4 : 0;

    return AnimatedContainer(
        duration: Duration(milliseconds: 500),
        curve: Curves.easeIn,
        margin: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top + 200,
            bottom: 50,
            right: 30),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.black38,
              blurRadius: blur,
              offset: Offset(offset, offset),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 350,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(data['image']),
                  )),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.only(left: 10),
              child: Text(
                data['title'],
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.black,
                ),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: 5),
            Container(
              padding: EdgeInsets.only(left: 10),
              child: Text(
                data['descricao'],
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            // color: Colors.yellow,
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                // ORANGE
                // Color(getColorHex('FFA500')),
                // Color(getColorHex('7FFFD4'))
                // BLUE
                Color(getColorHex('0CE7F0')),
                Color(getColorHex('0098D9')),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.0, 1.0],
            )),
            constraints: BoxConstraints.expand(),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + 5),
                  child: Container(
                    height: 180,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/logo.png'))),
                  ),
                ),
                Container(
                  child: PageView.builder(
                    controller: controller,
                    physics: const BouncingScrollPhysics(),
                    itemCount: menuData.length,
                    itemBuilder: (context, int currentIndex) {
                      bool active = currentIndex == currentPage;
                      return GestureDetector(
                        child: _buildStoryPage(menuData[currentIndex], active),
                        onTap: () => Navigator.of(context).push(
                              PageRouteBuilder(
                                pageBuilder: (_, __, ___) =>
                                    menuData[currentIndex]['page'],
                                transitionsBuilder: (context, animation,
                                        secondaryAnimation, child) =>
                                    FadeTransition(
                                        opacity: animation, child: child),
                              ),
                            ),
                      );
                    },
                  ),
                ),
              ],
            )));
  }
}
