import 'package:app_event/ui/common/card_speaker.dart';
import 'package:app_event/ui/detail/detail_speaker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SpeakersPage extends StatefulWidget {
  @override
  _SpeakersPageState createState() => _SpeakersPageState();
}

class _SpeakersPageState extends State<SpeakersPage> {
  Future<QuerySnapshot> _data;

  Future<QuerySnapshot> getSpeakers() async {
    var firestore = Firestore.instance;

    QuerySnapshot querySnapshot =
        await firestore.collection('speakers').getDocuments();

    return querySnapshot;
  }

  @override
  void initState() {
    super.initState();
    _data = getSpeakers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Palestrantes".toUpperCase(), style: TextStyle(letterSpacing: 5),),
          centerTitle: true,
        ),
        body: Container(
          child: FutureBuilder<QuerySnapshot>(
              future: _data,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) return Text('Error: ${snapshot.error}');
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  default:
                    return ListView.builder(
                      padding: EdgeInsets.all(8),
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          child: CardSpeaker(snapshot.data.documents[index]),
                          onTap: () => Navigator.of(context).push(
                                PageRouteBuilder(
                                  pageBuilder: (_, __, ___) => DetailSpeaker(
                                      snapshot.data.documents[index]),
                                  transitionsBuilder: (context, animation,
                                          secondaryAnimation, child) =>
                                      FadeTransition(
                                          opacity: animation, child: child),
                                ),
                              ),
                        );
                      },
                     );
                }
              }),
        ));
  }
}
