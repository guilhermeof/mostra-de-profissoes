import 'package:app_event/models/course_model.dart';
import 'package:app_event/ui/common/card_list_courses.dart';
import 'package:flutter/material.dart';

class CoursesList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/img_cursos.jpg'),
                          fit: BoxFit.cover)),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 100, left: 10),
                    child: Text(
                      'Cursos ofertados pela UEMA',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 230),
                      height: MediaQuery.of(context).size.height - 230,
                      child: CustomScrollView(
                        scrollDirection: Axis.vertical,
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: false,
                        slivers: <Widget>[
                          SliverPadding(
                            padding: EdgeInsets.symmetric(vertical: 0),
                            sliver: SliverList(
                                delegate: SliverChildBuilderDelegate(
                                    (context, index) => CardListCourse(
                                          curso: cursos[index],
                                        ),
                                    childCount: cursos.length)),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                _backButton(context)
              ],
            )
          ],
        ),
      ),
    );
  }

  Container _backButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: BackButton(color: Colors.white),
    );
  }
}
