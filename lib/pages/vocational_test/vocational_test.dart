import 'package:app_event/helpers/color_hex.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class VacationalTest extends StatefulWidget {
  @override
  _VacationalTestState createState() => _VacationalTestState();
}

class _VacationalTestState extends State<VacationalTest> {
  // final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";

  @override
  Widget build(BuildContext context) {
    // mWebView.getSettings().setUserAgentString(USER_AGENT);
    return WebviewScaffold(
      appBar: AppBar(
        title: Text('Teste Vocacional'.toUpperCase()),
        centerTitle: true,
        backgroundColor: Color(getColorHex('275273')),
      ),
      url:
          // 'https://especiais.g1.globo.com/educacao/guia-de-carreiras/2017/teste-vocacional/',
          // 'https://google.com/',
          'https://querobolsa.com.br/teste-vocacional-gratis',
          // 'https://www.arealme.com/career/pt/',
      withZoom: true,
    );
  }
}
