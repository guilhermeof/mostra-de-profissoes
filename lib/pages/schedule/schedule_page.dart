import 'package:app_event/helpers/color_hex.dart';
import 'package:app_event/helpers/schedule.dart';
import 'package:app_event/pages/schedule/schedule_list.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SchedulePage extends StatefulWidget {
  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  QuerySnapshot initalData;
  AppLifecycleState appLifecycleState;

  @override
  void initState() {
    super.initState();
    setState(() {
        _controller = TabController(vsync: this, length: scheduleData.length, initialIndex: 0);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: _body(scheduleData)
        );
  }

  _body(List<Schedule> snapshot) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Programação".toUpperCase(),
            style: TextStyle(),
          ),
          centerTitle: true,
          bottom: TabBar(
            indicatorColor: Color(getColorHex('FF5747')),
            // labelStyle: TextStyle(fontSize: 14),
            // indicatorWeight: 8,
            // indicatorSize: TabBarIndicatorSize.tab,
            // unselectedLabelColor: Colors.white,
            // labelColor: Color(getColorHex('FF5747')),
            // indicator: BoxDecoration(
            // color: Colors.white,
            // shape: BoxShape.circle,
            // ),
            controller: _controller,
            isScrollable: true,
            tabs: snapshot.map((schedule) {
              return Tab(
                text: schedule.dia.toUpperCase(),
              );
            }).toList(),
          ),
        ),
        body: Container(
          color: Colors.grey[50],
          child: TabBarView(
            controller: _controller,
            children: snapshot.map((schedule) {
              return ScheduleList(sessions: schedule.sessions);
            }).toList(),
          ),
        ));
  }
}

// PreferredSize(
//             preferredSize: Size.fromHeight(80.0),
//             child: Container(
//               padding: EdgeInsets.only(bottom: 20),
//               child: TabBar(
//                 labelStyle: TextStyle(fontSize: 14),
//                 indicatorWeight: 8,
//                 indicatorSize: TabBarIndicatorSize.tab,
//                 unselectedLabelColor: Colors.white,
//                 labelColor: Color(getColorHex('FF5747')),
//                 indicator: BoxDecoration(
//                   color: Colors.white,
//                   shape: BoxShape.circle,
//                 ),
//                 controller: _controller,
//                 isScrollable: true,
//                 tabs: snapshot.data.map((schedule) {
//                   return Tab(
//                       child: Center(
//                     child: Container(
//                         padding: EdgeInsets.all(5.0),
//                         child: Column(
//                           mainAxisSize: MainAxisSize.min,
//                           children: <Widget>[
//                             Text(
//                               schedule.dia.toUpperCase(),
//                               style: TextStyle(
//                                   fontSize: 20, fontWeight: FontWeight.w300),
//                             ),
//                             Text(
//                               schedule.mes.toUpperCase(),
//                               style: TextStyle(
//                                 fontSize: 10,
//                               ),
//                             )
//                           ],
//                         )),
//                   ));
//                 }).toList(),
//               ),
//             ),
//           ),
