import 'package:app_event/helpers/schedule.dart';
import 'package:app_event/ui/common/card_schedule.dart';
import 'package:flutter/material.dart';


class ScheduleList extends StatelessWidget {
  final List<Sessions> sessions;

  ScheduleList({this.sessions});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomScrollView(
        scrollDirection: Axis.vertical,
        shrinkWrap: false,
        slivers: <Widget>[
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10),
            sliver: SliverList(
                delegate: SliverChildBuilderDelegate((context, index) => CardSchedule(sessions[index]), childCount: sessions.length)),
          )
        ],
      ),
    );
  }
}

