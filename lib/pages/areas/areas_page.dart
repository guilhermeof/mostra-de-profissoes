import 'package:app_event/models/area_model.dart';
import 'package:app_event/ui/common/card_courses.dart';
import 'package:flutter/material.dart';

class AreasPage extends StatefulWidget {
  @override
  _AreasPageState createState() => _AreasPageState();
}

class _AreasPageState extends State<AreasPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/rocket.png'),
                        fit: BoxFit.cover
                      )
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 80, left: 150),
                      child: Text("Áreas de Atuação", style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                      ),),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 220, left: 10, right: 10, bottom: 10),
                        height: MediaQuery.of(context).size.height - 230,
                        child: GridView.builder(
                      shrinkWrap: false,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: cursos.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 5,
                          crossAxisCount: 2,
                          childAspectRatio: MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 2.5)),
                      itemBuilder: (BuildContext context, int index) {
                        return CardCourse(area: cursos[index]);
                      },
                    ),
                      )
                    ],
                  ),
                  _backButton(context)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Container _backButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: BackButton(color: Colors.white),
    );
  }
}

// Container(
//                     color: Colors.white,
//                     padding: EdgeInsets.only(
//                         top: MediaQuery.of(context).size.height - 500),
//                     height: MediaQuery.of(context).size.height,
//                     child: GridView.builder(
//                       shrinkWrap: false,
//                       physics: const NeverScrollableScrollPhysics(),
//                       itemCount: cursos.length,
//                       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                         crossAxisSpacing: 28,
//                         mainAxisSpacing: 20,
//                           crossAxisCount: 2,
//                           childAspectRatio: MediaQuery.of(context).size.width /
//                               (MediaQuery.of(context).size.height / 2.5)),
//                       itemBuilder: (BuildContext context, int index) {
//                         return CardCourse(area: cursos[index]);
//                       },
//                     ),
//                   ),