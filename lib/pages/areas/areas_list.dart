import 'package:app_event/models/area_model.dart';
import 'package:app_event/ui/common/card_list_courses_area.dart';
import 'package:flutter/material.dart';

class AreasList extends StatelessWidget {
  final List<Curso> cursos;
  final Area area;

  AreasList({this.cursos, this.area});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/images/rocket.png'),
                          fit: BoxFit.cover)),
                          child: Padding(
                      padding: const EdgeInsets.only(top: 80, left: 100),
                      child: Text(area.area, style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                      ),textAlign: TextAlign.center,),
                    ),
                ),
                // Container(
                //   width: double.infinity,
                //   height: 300,
                //   child: Column(
                //     crossAxisAlignment: CrossAxisAlignment.center,
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: <Widget>[
                //        Text(
                //           area.area,
                //           style: TextStyle(
                //               color: Colors.white,
                //               fontWeight: FontWeight.bold,
                //               fontSize: 40),
                //               textAlign: TextAlign.center,
                //         ),
                //     ],
                //   ),
                // ),
                Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 230),
                      height: MediaQuery.of(context).size.height - 230,
                      child: CustomScrollView(
                        scrollDirection: Axis.vertical,
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: false,
                        slivers: <Widget>[
                          SliverPadding(
                            padding: EdgeInsets.symmetric(vertical: 0),
                            sliver: SliverList(
                                delegate: SliverChildBuilderDelegate(
                                    (context, index) => CardListCourseArea(
                                          curso: cursos[index],
                                        ),
                                    childCount: cursos.length)),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                _backButton(context)
              ],
            )
          ],
        ),
      ),
    );
  }

  Container _backButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: BackButton(color: Colors.white),
    );
  }
}
