import 'package:app_event/helpers/color_hex.dart';
import 'package:app_event/models/area_model.dart';
import 'package:app_event/ui/styles/text_style.dart';
import 'package:flutter/material.dart';

class CourseDetailPage extends StatefulWidget {
  final Curso curso;

  const CourseDetailPage({Key key, this.curso}) : super(key: key);
  @override
  _CourseDetailPageState createState() => _CourseDetailPageState();
}

class _CourseDetailPageState extends State<CourseDetailPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        height: double.infinity,
        child: Stack(
          children: <Widget>[
            Hero(
              tag: "${widget.curso.nome + widget.curso.modalidade}",
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: <Color>[
                      Color(getColorHex('0CE7F0')),
                      Color(getColorHex('0098D9')),
                    ],
                    stops: [0.0, 0.9],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(0.0, 1.0),
                  ),
                ),
              ),
            ),
            Container(
              height: 200,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(getColorHex('0CE7F0')),
                    Color(getColorHex('0098D9')),
                  ],
                  stops: [0.0, 0.9],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(0.0, 1.0),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.curso.nome,
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 170),
              height: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadiusDirectional.only(
                      topStart: Radius.circular(25),
                      topEnd: Radius.circular(25))),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    AnimatedBuilder(
                      animation: _controller,
                      builder: (context, widget) => Transform.translate(
                            transformHitTests: false,
                            offset: Offset.lerp(Offset(0.0, 200.0), Offset.zero,
                                _controller.value),
                            child: widget,
                          ),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 30, top: 10, bottom: 10, right: 30),
                            child: Text(
                              widget.curso.descricao,
                              style: TextStyle(
                                  height: 1.5,
                                  color: Colors.black87,
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w400),
                              textAlign: TextAlign.justify,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 30, top: 10, bottom: 10),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Modalidade:",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                      color: widget.curso.modalidade == 'EaD'
                                          ? Colors.orange
                                          : Colors.blue,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 3, horizontal: 10),
                                    child: Text(
                                      widget.curso.modalidade,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          _radiusButton(label: "Duração:", color: Colors.indigo, content: "${widget.curso.duracao} anos"),
                          widget.curso.cargahoraria != null
                              ? _radiusButton(label: "Carga horária:", color: Colors.green, content: "${widget.curso.cargahoraria} horas")
                              : Container(),
                          widget.curso.local != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 30, top: 10, bottom: 10),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        "Locais de oferta:",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                          child: Container(
                                              child: Text(
                                        widget.curso.local,
                                        softWrap: true,
                                      )))
                                    ],
                                  ),
                                )
                              : Container()
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            _closeButton(context)
          ],
        ),
      ),
    );
  }

  Container _closeButton(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: IconButton(
        icon: const Icon(
          Icons.close,
          color: Colors.white,
        ),
        tooltip: MaterialLocalizations.of(context).closeButtonTooltip,
        onPressed: () {
          Navigator.maybePop(context);
        },
      ),
    );
  }

  Padding _radiusButton({String label, Color color, String content}) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, top: 10, bottom: 10),
      child: Row(
        children: <Widget>[
          Text(
            label,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
              child: Text(
                content,
                style: TextStyle(fontSize: 12, color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
