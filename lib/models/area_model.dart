class Area{
  String area;
  String imagePaht;
  List<Curso> cursos;

  Area({this.area, this.cursos, this.imagePaht});
}

class Curso{
  String nome;
  String descricao;
  String modalidade;
  String local;
  String duracao;
  String cargahoraria;

  Curso({this.nome, this.descricao, this.modalidade, this.local, this.duracao, this.cargahoraria});
}

final cursos = [
  Area(
    area: "Tecnológica",
    imagePaht: 'assets/images/tec.jpg',
    cursos: <Curso>[
      Curso(
        nome: "Arquitetura e Urbanismo Bacharelado",
        descricao: "O Curso de Arquitetura e Urbanismo forma profissionais para atuar em diversos campos, como: projeto de conjuntos arquitetônicos e monumentos; arquitetura paisagística e de interiores; planejamento físico, local, urbano e regional; desenvolvimento urbano e regional; paisagismo e trânsito, e serviços afins e correlatos. O mercado de trabalho inclui empresa de construção civil, órgão de planejamento, empresas públicas, escritórios particulares, firmas de arquitetura de interiores, mobiliário, desenho industrial e programa visual.",
        modalidade: "Presencial",
        local: null,
        duracao: "5",
        cargahoraria: null
      ),
      Curso(
        nome: "Curso Superior de Tecnologia em Segurança no Trabalho",
        descricao: "Empresas e instituições industriais, construção civil, mineração, produção e distribuição de energia, petróleo e gás e outras empresas. Pode também, exercer atividades relacionadas à gestão e liderança de projetos e sistemas de segurança no trabalho, tais como assessoria e consultoria em saúde e segurança, gestão ambiental, planejamento, implantação, operação, avaliação, auditoria, controle e supervisão de condições de trabalho e instalação de tecnologias de campo.",
        modalidade: "EaD",
        local: 'Bacabal, Balsas, Barra do Corda, Caxias, Codó, Coroatá, Grajaú, Imperatriz, Pinheiro, Santa Inês, São Luis, Timom.',
        duracao: "3",
        cargahoraria: "2625"
      ),
      Curso(
        nome: "Engenharia Civil Bacharelado",
        descricao: "O Curso de Engenharia Civil apresenta-se como o ramo da engenharia responsável pelo projeto de construção de obras como casas, edifícios, pontes, túneis, viadutos e estradas. O profissional de Engenharia Civil pode exercer atividades de engenheiro de obras, engenheiro de fiscalização e engenheiro projetista. Essas habilitações também podem estar vinculadas ao ensino e ao desenvolvimento em pesquisas. O campo de atuação desses profissionais abrange empresas de consultoria, de projetos, empreiteiras, órgãos governamentais, instituições de ensino e pesquisa, empresas públicas, privadas e de economia mista.",
        modalidade: "Presencial",
        local: null,
        duracao: "5",
        cargahoraria: null
      ),
      Curso(
        nome: "Engenharia da Computação Bacharelado",
        descricao: "O engenheiro da computação é um profissional com formação plena em Engenharia, preparado em assuntos de computação para especificar, conceber, desenvolver, implementar, adaptar, produzir, industrializar, instalar e manter sistemas computacionais, bem como perfazer a integração de recursos físicos e lógicos necessários para o atendimento das necessidades informacionais, computacionais e da automação de organização em geral, devendo ter, para isso, uma sólida formação técnico-científica e profissional geral que o capacite a absorver e desenvolver novas tecnologias, estimulando a sua atuação crítica e criativa na identificação e resolução de problemas, considerando seus aspectos político-econômicos, sociais ambientais e culturais, com visão ética e humanista, em atendimento às demandas da sociedade.",
        modalidade: "Presencial",
        local: null,
        duracao: "4,5",
        cargahoraria: null
      ),
      Curso(
        nome: "Engenharia de Produção Bacharelado",
        descricao: "O Curso de Engenharia de Produção objetiva formar profissionais capacitados a atuar na gestão de sistemas de produção que, numa definição genérica, são sistemas destinados a gerar bens e serviços para o mercado consumidor.",
        modalidade: "Presencial",
        local: null,
        duracao: "5",
        cargahoraria: null
      ),
      Curso(
        nome: "Formação de Oficiais Bombeiro Militar- Bacharelado em Segurança Pública do Trabalho - CBMMA",
        descricao: "O CFO-CBMMA é um curso destinado a formar bombeiros militares ao posto inicial de Aspirante a Oficial Bombeiro da Carreira Militar até alcançar o posto de Capitão, capacitando-os a desempenhar atribuições de comando, chefia e liderança sobre integrantes da tropa de bombeiros, nos serviços operacionais afetos à gerência de pessoal e de atividades-meio no âmbito administrativo e em ações tipicamente militares.",
        modalidade: "Presencial",
        local: null,
        duracao: "3",
        cargahoraria: null
      ),
      Curso(
        nome: "Engenharia Mecânica Bacharelado",
        descricao: "O Curso de Engenharia Mecânica é o ramo da engenharia responsável pela elaboração de projetos, implantação, operação, manutenção de instalações mecânicas diversas (oficinas, indústrias, fabricação de máquinas e produtos mecânicos, fábrica em geral). ",
        modalidade: "Presencial",
        local: null,
        duracao: "4,5",
        cargahoraria: null
      ),
    ]
  ),
   Area(
    area: "Ciências Sociais",
    imagePaht: 'assets/images/tec.jpg',
    cursos: <Curso>[
       Curso(
        nome: "Administração Pública",
        descricao: "Atuar e desenvolver atividades específicas da gestão nas organizações públicas e participar da elaboração, do planejamento, da coordenação e do controle de políticas públicas.",
        modalidade: "EaD",
        local: 'Açailândia, Alto Parnaiba, Balsas, Barra do Corda, Carolina, Carutapera, Codó, Coelho Neto, Colinas, Coroatá, Fortaleza dos Nogueiras, Grajaú, Itapecuru Mirim, Pedreiras, Pinheiro, Porto Franco, Santa Inês, São Bento, São João dos Patos, São Luís, Timom, Viana, Zé Doca.',
        duracao: "4",
        cargahoraria: "3135"
      ),
      Curso(
        nome: "Administração Bacharelado",
        descricao: "O objetivo do Curso de Administração é coordenar, controlar e dirigir empresas, buscando os melhores resultados em termos de produtividade. Administração é um curso de preparação profissional que habilita a gerir recursos de toda ordem, mediante o exercício do planejamento, organização, coordenação e controle, no âmbito das organizações de qualquer natureza ou parte, com vistas à otimização de seus resultados.",
        modalidade: "Presencial",
        local: null,
        duracao: "4,5",
        cargahoraria: null
      ),
      Curso(
        nome: "Direito Bacharelado",
        descricao: "O Curso de Direito da UEMA busca formar egressos generalistas, com aptidão para apreensão de conceitos,  argumentos, domínio das competências e habilidades para sua futura área de atuação, seja como advogado, juiz de direito, promotor de justiça, procurador, delegado, entre outras opções que permite o Curso de Bacharelado em Direito.",
        modalidade: "Presencial",
        local: null,
        duracao: "5",
        cargahoraria: null
      ),
      Curso(
        nome: "Ciências Contábeis Bacharelado",
        descricao: "O Bacharel em Ciências Contábeis ou Contador atua na contabilidade das empresas e instituições. Em sua atividade, desenvolve visão global e dinâmica dos ambientes econômicos e financeiros, retratando as influências destes nas alterações patrimoniais, por meio da identificação, da mensuração, do registro, da divulgação e da análise dos eventos e das transações ocorridas nas organizações.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Formação de Oficiais – Bacharelado em Segurança Pública - PMMA",
        descricao: "O CFO é um curso destinado a formar Oficiais Policiais Militares ao nível de 2º Tenente até o Posto de Capitão, capacitando-os a desempenhar atribuições de comando, chefia e liderança sobre integrantes Militares, nos serviços operacionais afetos à gerência de pessoal e de atividades-meio no âmbito administrativo e em ações tipicamente militares.",
        modalidade: "Presencial",
        local: null,
        duracao: "3,5",
        cargahoraria: null
      ),
      Curso(
        nome: "Ciências Sociais Bacharelado ou Licenciatura",
        descricao: "O Curso de Ciências Sociais visa formar cientistas sociais capazes de produzir conhecimentos sobre a realidade com vista a colaborar e interferir no desenvolvimento sociopolítico do país e em especial do  Estado do Maranhão.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Curso Superior de Tecnologia em Gestão Comercial",
        descricao: "Articula e decide sobre as vendas em negócios diversos. Realiza estudos de viabilidade econômica, financeira e tributária. Elabora análises comerciais considerando as demandas e oportunidades do mercado. Planeja pesquisas de mercado. Gerencia sistemas de informações comerciais. Avalia e emite parecer técnico em sua área de formação.",
        modalidade: "Presencial",
        local: null,
        duracao: "2",
        cargahoraria: null
      ),
      Curso(
        nome: "Curso Superior de Tecnologia em Gestão Comercial",
        descricao: "Formulação e implantação de políticas públicas voltadas para o desenvolvimento do comércio.Aplicação, desenvolvimento, pesquisa aplicada e inovação científico-tecnológica nos processos de gestão comercial. Difusão de tecnologias de gestão que atendam às necessidades dos clientes, fornecedores, entidades e demais interessados na melhoria da gestão comercial.",
        modalidade: "EaD",
        local: 'Açailândia, Bacabal, Balsas, Barra do Corda, Codó, Coelho Neto, Imperatriz, Itapecuru Mirim, Lago da Pedra, Pinheiro, Presidente Dutra, Santa Inês, São Bento, São João dos Patos, São Luís, Timom, Zé Doca.',
        duracao: "2",
        cargahoraria: "1830"
      ),
    ]
  ),
   Area(
    area: "Ciências Agrárias",
    imagePaht: 'assets/images/tec.jpg',
    cursos: <Curso>[
      Curso(
        nome: "Curso Superior de Tecnologia em Alimentos",
        descricao: "Indústrias alimentícias de produtos agroindustriais;Empresas de armazenamento e distribuição de alimentos;Indústrias de aproveitamento de resíduos;Instituições de pesquisas científicas e tecnológicas, como colaborador e como professor de disciplinas de sua área de formação;",
        modalidade: "EaD",
        local: 'Açailândia, Caxias, Coelho Neto, Colinas, Itapecuru Mirim, Lago da Pedra, Porto Franco, São Bento, São Luís, Zé Doca.',
        duracao: "3",
        cargahoraria: "2610"
      ),
      Curso(
        nome: "Agronomia Bacharelado",
        descricao: "O Curso tem por objetivo formar profissionais capazes de promover, orientar e administrar a utilização dos fatores de produção, visando racionalizar a produção vegetal e animal, em harmonia com o ecossistema. O Curso de Agronomia prepara profissionais para planejar e dirigir serviços relativos à área rural, irrigação e drenagem, construções rurais, topografia e aerofotogrametria; elaborar, assessorar e executar projetos que visam à implantação de novos métodos e práticas agrícolas.",
        modalidade: "Presencial",
        local: null,
        duracao: "5",
        cargahoraria: null
      ),
      Curso(
        nome: "Engenharia de Pesca Bacharelado",
        descricao: "O curso tem como objetivo qualificar profissionais em Engenharia de Pesca, com conhecimentos teóricos e práticos nos âmbitos de criação, manejo, nutrição, alimentação, reprodução e melhoramento de animais aquáticos, captura e conservação do pescado, bem como socializar conhecimentos em busca de melhorias ao homem.",
        modalidade: "Presencial",
        local: null,
        duracao: "5,5",
        cargahoraria: null
      ),
      Curso(
        nome: "Medicina Veterinária Bacharelado",
        descricao: "É um curso de preparação profissional para cuidar de animais domésticos, rebanhos e criações, fazer exames clínicos, diagnósticos e prescrever tratamentos; controlar as condições de higiene em produtos de origem animal, destinados à alimentação, fiscalizar processamentos e comercialização dos produtos de origem animal e diversos; atuar na prevenção e controle das zoonoses.",
        modalidade: "Presencial",
        local: null,
        duracao: "5",
        cargahoraria: null
      ),
      Curso(
        nome: "Zootecnia Bacharelado",
        descricao: "O curso tem finalidade de formar profissionais para prestar assistência, assessoria e consultoria nas áreas de produção animal caracterizada como o seu campo prioritário de atuação. Este profissional busca o melhoramento genético dos animais, aliado à nutrição e alimentação animal, desenvolvendo, ainda, a capacidade de gerar e aplicar conhecimentos científicos às criações, explorando-as economicamente, objetivando maior produtividade e lucratividade.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Curso Superior de Tecnologia em Gestão de Agronegócio",
        descricao: "Planeja, projeta e executa empreendimentos voltados para o agronegócio. Projeta mercados estratégicos para o agronegócio. Analisa indicadores de mercado. Afere o desempenho da produção no agronegócio. Analisa e controla custos de produção do agronegócio. Caracteriza e interpreta as diversas cadeias produtivas do agronegócio. Planeja e executa a implantação de arranjos produtivos locais. Gerencia empresas/propriedades rurais. Avalia e emite parecer técnico em sua área de formação.",
        modalidade: "Presencial",
        local: null,
        duracao: "3",
        cargahoraria: null
      ),
      Curso(
        nome: "Curso Superior de Tecnologia em Gestão Ambiental",
        descricao: "O tecnólogo em Gestão Ambiental planeja, gerencia e executa as atividades de diagnóstico, avaliação de impacto, proposição de medidas mitigadoras – corretivas e preventivas – recuperação de áreas degradadas, acompanhamento e monitoramento da qualidade ambiental.",
        modalidade: "Presencial",
        local: null,
        duracao: "2,5",
        cargahoraria: null
      ),
      Curso(
        nome: "Curso Superior de Tecnologia em Alimentos",
        descricao: "Planeja, implanta, executa e avalia os processos relacionados ao beneficiamento, industrialização e conservação de alimentos e bebidas. Gerencia os processos de produção e industrialização de alimentos. Supervisiona as várias fases dos processos de industrialização e desenvolvimento de alimentos. Realiza análise microbiológica, bioquímica, físico-química, microscópica, sensorial, toxicológica e ambiental na produção de alimentos.",
        modalidade: "Presencial",
        local: null,
        duracao: "3",
        cargahoraria: null
      ),
      Curso(
        nome: "Curso Superior de Tecnologia em Fruticultura",
        descricao: "Planeja, projeta, gerencia e executa empreendimentos voltados para a produção de frutas. Elabora e  executa projetos agrícolas que compreendem a implantação, cultivo, produção, colheita e pós-colheita de frutas. Fiscaliza, elabora relatórios e pareceres sobre o controle de qualidade, classificação e certificação de frutas. Planeja e executa a implantação de viveiros de mudas. Orienta o manejo de solo, adubação e receituários técnicos para a fruticultura. Gerencia equipes técnicas na área.",
        modalidade: "Presencial",
        local: null,
        duracao: "2,5",
        cargahoraria: null
      ),
    ]
  ),
  Area(
    area: "Ciências Exatas e Naturais",
    imagePaht: 'assets/images/tec.jpg',
    cursos: <Curso>[
      Curso(
        nome: "Ciências Naturais Licenciatura",
        descricao: "O Licenciado em Ciências Naturais é o professor que planeja, organiza e desenvolve atividades e materiais relativos à Educação em Ciências. Sua atribuição central é a docência na Educação Básica, que requer sólidos conhecimentos sobre os fundamentos da Física, da Química e da Biologia, sobre seus desenvolvimentos históricos e suas inter-relações; assim como sobre estratégias para transposição do conhecimento das Ciências Naturais em saber escolar.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Química Licenciatura",
        descricao: "O Licenciado em Química é um profissional com formação abrangente para trabalhar nos diversos campos da Química e em todas as suas modalidades fundamentais, além de possuir domínio das técnicas básicas de utilização de laboratórios e seus equipamentos, possuir conhecimentos humanísticos e pedagógicos que lhe habilite a exercer plenamente sua cidadania e a profissão de educador.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Física Licenciatura",
        descricao: "Docência na Educação Básica, que requer sólidos conhecimentos sobre os fundamentos da Física, sobre seu desenvolvimento histórico e suas relações com diversas áreas, assim como sobre estratégias para transposição do conhecimento da Física em saber escolar.",
        modalidade: "EaD",
        local: 'Bacabal, Balsas, Barra do Corda, Coelho Neto, Colinas, São Luís, Vitorino Freire, Zé Doca.',
        duracao: "4",
        cargahoraria: "3465"
      ),
      Curso(
        nome: "Física Licenciatura",
        descricao: "O Licenciado em Física, apoiado em conhecimentos sólidos e atualizados em Física, deve ser capaz de abordar e tratar problemas novos e tradicionais e deve estar sempre preocupado em buscar novas formas do saber e do fazer científico ou tecnológico. Em todas suas atividades, a atitude de investigação deve estar sempre presente, embora associada a diferentes formas e objetos de trabalho.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Ciências Biológicas Licenciatura",
        descricao: "O Licenciado em Ciências Biológicas é o professor que planeja, organiza e desenvolve atividades e materiais relativos ao Ensino de Biologia. Sua atribuição central é a docência na Educação Básica, que requer sólidos conhecimentos sobre os fundamentos da Biologia, sobre seu desenvolvimento histórico e suas relações com diversas áreas; assim como sobre estratégias para transposição do conhecimento biológico em saber escolar. Além de trabalhar diretamente na sala de aula, o licenciado elabora e analisa materiais didáticos, como livros, textos, vídeos, programas computacionais, ambientes virtuais de aprendizagem, entre outros.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Matemática Licenciatura",
        descricao: "O Curso de Licenciatura em Matemática propõe-se a formar professores para o Ensino Fundamental, Médio e Superior. Deverá ter consciência de matemático junto a sua comunidade e de sua responsabilidade como educador nos vários contextos de sua atuação profissional, assim como: deverá ter uma formação básica ampla e sólida com a adequada fundamentação teórico-prática necessária para o bom desenvolvimento de sua prática educativa.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
    ]
  ),
   Area(
    area: "Educação",
    imagePaht: 'assets/images/tec.jpg',
    cursos: <Curso>[
      Curso(
        nome: "Letras Licenciatura",
        descricao: "O Licenciado em Letras é o profissional que investiga e utiliza a linguagem e a literatura (nacional ou estrangeira) como instrumento teórico indispensável ao exercício de suas atividades profissionais. Tem como principal atividade proporcionar os meios de comunicação do corpo discente, enquanto professor de Língua e Literatura Nacional e Estrangeira, em sua forma escrita ou falada.",
        modalidade: "Presencial",
        local: null,
        duracao: "4,5",
        cargahoraria: null
      ),
      Curso(
        nome: "Pedagogia Licenciatura",
        descricao: "Docência na Educação Infantil e nos anos iniciais do Ensino Fundamental;Elabora e analisa materiais didáticos, como livros, textos, vídeos, programas computacionais, ambientes virtuais de aprendizagem, entre outros;Realiza ainda pesquisas em Educação Básica, coordena e supervisiona equipes de trabalho.",
        modalidade: "EaD",
        local: 'Açailândia, Alto Parnaiba, Anapurus, Arari, Bacabal, Balsas, Bom Jesus das Selvas, Carolina, Cândido Mendes, Carutapera, Caxias, Codó, Coelho Neto, Colinas, Coroatá, Dom Pedro, Fortaleza dos Nogueiras, Grajaú, Humberto de Campos, Imperatriz, Itapecuru Mirim, Lago da Pedra, Nina Rodrigues, Paraibano, Pedreiras, Penalva, Porto Franco, Presidente Dutra, Santa Inês, São Bento, São João dos Patos, Santa Quiteria do Maranhão, São Luis, Timbiras, Timom, Viana, Zé Doca.',
        duracao: "4",
        cargahoraria: "3435"
      ),
      Curso(
        nome: "Pedagogia Licenciatura",
        descricao: "O Curso de Licenciatura em Pedagogia está estruturado para possibilitar ao futuro pedagogo ser capaz de: atuar na docência da educação básica de maneira competente e com compromisso profissional; desenvolver atividades profissionais em assessoria técnico-pedagógica nas escolas de educação básica; atuar na seleção e qualificação de recursos humanos para entidades governamentais, não-governamentais e empresas privadas; desenvolvimento e avaliação dos projetos educativos da escola; optar por procedimentos metodológicos adequados às especificidades das diversas áreas do conhecimento e características dos alunos; socializar as produções e reflexões da prática pedagógica.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Filosofia Licenciatura",
        descricao: "Escolas públicas e privadas, transmitindo aos alunos o legado da tradição e o gosto pelo pensamento inovador, crítico e independente.Podem contribuir profissionalmente também em outras áreas, no debate interdisciplinar, nas assessorias culturais etc.",
        modalidade: "EaD",
        local: 'Arari, Bacabal, Balsas, Barra do Corda, Carutapera, Caxias, Dom Pedro, Fortaleza dos Nogueiras, Humberto de Campos, Pedreiras, Penalva, Porto Franco, Santa Inês, São Luís, Timom, Viana.',
        duracao: "4",
        cargahoraria: "3645"
      ),
      Curso(
        nome: "Filosofia Licenciatura",
        descricao: "O Licenciado em Filosofia é o professor que planeja, organiza e desenvolve atividades e materiais relativos ao Ensino de Filosofia. Sua atribuição central é a docência na Educação Básica, que requer sólidos conhecimentos sobre os fundamentos da Filosofia, sobre seu desenvolvimento histórico e suas relações com diversas áreas; assim como sobre estratégias para transposição do conhecimento filosófico em saber escolar.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "História Licenciatura",
        descricao: "Os profissionais graduados em História estão habilitados a exercerem docência no Ensino Fundamental, Médio e Superior. As áreas de atuação do Licenciado em História são promissoras, seja por meio de consultoria, assessoramento, extensão, pesquisa em arquivos, museus, perícia técnica e administração pública. Os historiadores têm cada vez mais possibilidades de contribuir com seus conhecimentos nas mais diversas áreas do saber.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Geografia Licenciatura",
        descricao: "Docência na Educação Básica, que requer sólidos conhecimentos sobre os fundamentos da Geografia, sobre seu desenvolvimento histórico e suas relações com diversas áreas; assim como sobre estratégias para transposição do conhecimento geográfico em saber escolar.",
        modalidade: "EaD",
        local: 'Açailândia, Alto Parnaiba, Anapurus, Arari, Bacabal, Balsas, Barra do Corda, Bom Jesus das Selvas, Carolina, Carutapera, Codó, Coelho Neto, Colinas, Dom Pedro, Fortaleza dos Nogueiras, Humberto de Campos, Imperatriz, Itapecuru Mirim, Lago da Pedra, Nina Rodrigues, Paraibano, Pedreiras, Pinheiro, Porto Franco, Presidente Dutra, Santa Inês, São João dos Patos, Santa Quiteria do Maranhão, Timbiras, Timom, Viana, Vitorino Freire, Zé Doca.',
        duracao: "4",
        cargahoraria: "3435"
      ),
      Curso(
        nome: "Geografia Licenciatura ou Bacharelado",
        descricao: "O Curso de Geografia, com duas modalidades cursadas, propõe-se a formar profissionais habilitados a exercerem a docência ou a realizar investigação científica e técnica nos campos específicos e gerais da Geografia. Para tal, é instituído o grau de Licenciado ou Bacharel na conclusão do curso. As competências e habilidades para o geógrafo, entre outras atividades, são principalmente relativas a levantamento, pesquisa, diagnóstico, planejamento, execução, experimentação, modelagem, exploração e monitoramento; emissão de pareceres técnicos, assim como, a elaboração de planos de uso e ocupação do solo e projetos específicos na área ambiental para elaboração de Estudos e Relatórios de Impactos Ambientais (EIAs e RIMAs).",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Música Licenciatura",
        descricao: "Seu campo de atuação é o espaço escolar, especificamente a educação básica, ampliando-se para outros espaços da formação formais e não formais.",
        modalidade: "EaD",
        local: 'Anapurus, Arari, Bacabal, Balsas, Barra do Corda, Cândido Mendes, Carutapera, Caxias, Codó, Coelho Neto, Colinas, Coroatá, Dom Pedro, Fortaleza dos Nogueiras, Humberto de Campos, Imperatriz, Itapecuru Mirim, Lago da Pedra, Nina Rodrigues, Pedreiras, Pinheiro, Porto Franco, Presidente Dutra, Santa Inês, São João dos Patos, Santa Quiteria do Maranhão, São Luis, Timbiras, Timom, Viana, Zé Doca.',
        duracao: "4",
        cargahoraria: "3435"
      ),
      Curso(
        nome: "Música Licenciatura",
        descricao: "O Licenciado em Música é o profissional habilitado para o magistério de Música no Ensino Fundamental e Ensino Médio, podendo, ainda, obter, junto ao MEC, Registro de Professor nas seguintes disciplinas: Canto Coral; História da Música e Linguagem e Estruturação Musical. O campo de trabalho se amplia oferecendo oportunidade no setor da iniciação musical.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
      Curso(
        nome: "Educação Física Licenciatura",
        descricao: "O Licenciado em Educação Física é o professor que planeja, organiza e desenvolve atividades e materiais relativos à Educação Física. Sua atribuição central é a docência na Educação Básica, que requer sólidos conhecimentos sobre os fundamentos da Educação Física, sobre seu desenvolvimento histórico e suas relações com diversas áreas; assim como sobre estratégias para transposição do conhecimento da Educação Física em saber escolar. Realiza ainda pesquisas em Educação Física, coordena e supervisiona equipes de trabalho. Em sua atuação, prima pelo desenvolvimento do educando, incluindo sua formação ética, a construção de sua autonomia intelectual e de seu pensamento crítico.",
        modalidade: "Presencial",
        local: null,
        duracao: "4",
        cargahoraria: null
      ),
    ]
  ),
   Area(
    area: "Saúde",
    imagePaht: 'assets/images/tec.jpg',
    cursos: <Curso>[
      Curso(
        nome: "Enfermagem Bacharelado",
        descricao: "Os profissionais enfermeiros podem desempenhar funções administrativas nos serviços de Enfermagem em instituições de saúde públicas e privadas. O enfermeiro é capaz de demonstrar, em suas ações profissionais, espírito crítico e atitude de investigação científica, promovendo estudos e pesquisas que visem à melhoria de saúde das comunidades assistidas e reconhecendo a importância do trabalho em grupo, integrando-se a equipes interdisciplinares e multiprofissionais em prol da saúde da população.",
        modalidade: "Presencial",
        local: null,
        duracao: "5",
        cargahoraria: null
      ),
      Curso(
        nome: "Medicina Bacharelado",
        descricao: "O curso tem por finalidade formar profissionais comprometidos com uma assistência integral ao indivíduo, à família e à comunidade. O curso dispõe, para isso, de uma integração das disciplinas básicas com os profissionais, a fim de proporcionar a aproximação entre as necessidades da prática médica e as respectivas informações anatomo fisiopatológicas, tão importante para o saber médico. Busca ainda possibilitar ao aluno a capacidade de trabalhar com compromisso científico, ético e social.",
        modalidade: "Presencial",
        local: null,
        duracao: "6",
        cargahoraria: null
      ),
    ]
  ),
];