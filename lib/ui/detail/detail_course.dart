import 'package:app_event/helpers/color_hex.dart';
import 'package:app_event/models/area_model.dart';
import 'package:flutter/material.dart';
import 'package:app_event/ui/common/separator.dart';
import 'package:app_event/ui/styles/text_style.dart';


class DetailCourse extends StatelessWidget {

  final List<Curso> areas;

  DetailCourse(this.areas);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        color: Color(getColorHex('FF9F6E')),
        child: SingleChildScrollView(
          child: Stack (
          children: <Widget>[
            _getBackground(),
            _getGradient(),
            _getContent(),
            _getToolbar(context),
          ],
        ),
        )
      ),
    );
  }

  Container _getBackground () {
    return Container(
     child: Image.asset('assets/images/speakers.jpg',
       fit: BoxFit.cover,
       height: 300.0,
     ),
      constraints: BoxConstraints.expand(height: 295.0),
    );
  }

  Container _getGradient() {
    return Container(
      margin: EdgeInsets.only(top: 190.0),
      height: 110.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Color(getColorHex('FF9F6E')).withOpacity(0.0),
            Color(getColorHex('FF9F6E')),
          ],
          stops: [0.0, 0.9],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(0.0, 1.0),
        ),
      ),
    );
  }

  Container _getContent() {
    final _overviewTitle = "Sobre".toUpperCase();
    return Container(
      padding: EdgeInsets.fromLTRB(0.0, 72.0, 0.0, 32.0),
      child: Column(
        // padding: EdgeInsets.fromLTRB(0.0, 72.0, 0.0, 32.0),
        children: <Widget>[
          // CardDetailCourse(speaker,
          //   horizontal: false,
          // ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 32.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(_overviewTitle,
                  style: Style.headerTextStyle,),
                Separator(),
                // Text(
                //     speaker['spk_description'], style: Style.commonTextStyle),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _getToolbar(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          top: MediaQuery
              .of(context)
              .padding
              .top),
      child: BackButton(color: Colors.white),
    );
  }
}