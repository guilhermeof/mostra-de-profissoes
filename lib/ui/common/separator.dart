import 'package:app_event/helpers/color_hex.dart';
import 'package:flutter/material.dart';

class Separator extends StatelessWidget {
  final Color color;

  Separator({this.color});
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 8.0),
        height: 2.0,
        width: 18.0,
        color: color ?? Color(getColorHex('FF5747')));
  }
}
