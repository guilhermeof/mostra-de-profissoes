import 'dart:ui';

import 'package:app_event/models/area_model.dart';
import 'package:app_event/pages/areas/areas_datail.dart';
import 'package:app_event/ui/styles/text_style.dart';
import 'package:flutter/material.dart';

class CardListCourseArea extends StatefulWidget {
  final Curso curso;

  const CardListCourseArea({Key key, this.curso}) : super(key: key);

  @override
  _CardListareastate createState() => _CardListareastate();
}

class _CardListareastate extends State<CardListCourseArea> {
  _goToDetail(Curso curso) {
    final page = CourseDetailPage(curso: curso);
    Navigator.of(context).push(
      PageRouteBuilder<Null>(
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return AnimatedBuilder(
                animation: animation,
                builder: (BuildContext context, Widget child) {
                  return Opacity(
                    opacity: animation.value,
                    child: page,
                  );
                });
          },
          transitionDuration: Duration(milliseconds: 400)),
    );
  }

  @override
  Widget build(BuildContext context) {
    final middleSection = Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              widget.curso.nome,
              style: Style.commonTextStyle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(height: 5,),
            Container(
              decoration: BoxDecoration(
                color: widget.curso.modalidade == 'EaD' ? Colors.orange : Colors.blue,
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                child: Text(widget.curso.modalidade, style: TextStyle(
                  fontSize: 12,
                  color: Colors.white
                ),),
              ),
            )
          ],
        ),
      ),
    );

    final cardAreaContent = Container(
        margin: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[middleSection],
        ));

    final courseAreaCard = ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: Container(
          child: cardAreaContent,
          height: 80.0,
          width: (MediaQuery.of(context).size.width / 2) - 10,
          decoration: BoxDecoration(
            color: Colors.grey[300].withOpacity(0.7),
          ),
        ),
      ),
    );

    return GestureDetector(
        onTap: () {
          _goToDetail(widget.curso);
        },
        child: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 8,
              horizontal: 15,
            ),
            child: Hero(
              tag: "${widget.curso.nome + widget.curso.modalidade}",
              child: Material(
                color: Colors.transparent,
                child: courseAreaCard,
              ),
            )));
  }
}
