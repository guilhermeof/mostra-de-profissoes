import 'dart:ui';

import 'package:app_event/models/course_model.dart';
import 'package:app_event/ui/styles/text_style.dart';
import 'package:flutter/material.dart';

class CardListCourse extends StatefulWidget {
  final Curso curso;

  const CardListCourse({Key key, this.curso}) : super(key: key);

  @override
  _CardListCursoState createState() => _CardListCursoState();
}

class _CardListCursoState extends State<CardListCourse> {
  @override
  Widget build(BuildContext context) {
    final middleSection = Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              widget.curso.name,
              style: Style.commonTextStyle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(height: 4,),
            Text(
              "Duração: ${widget.curso.duration} anos",
              style: TextStyle(fontSize: 12),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );

    final cardCursoContent = Container(
        margin: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[middleSection],
        ));

    final courseCard = ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: Container(
          child: cardCursoContent,
          height: 80.0,
          width: (MediaQuery.of(context).size.width) - 10,
          decoration: BoxDecoration(
            color: Colors.grey[300].withOpacity(0.7),
          ),
        ),
      ),
    );

    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 15,
      ),
      child: courseCard,
    );
  }
}
