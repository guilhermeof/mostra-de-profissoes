import 'dart:ui';

import 'package:app_event/models/area_model.dart';
import 'package:app_event/pages/areas/areas_list.dart';
import 'package:app_event/ui/styles/text_style.dart';
import 'package:flutter/material.dart';

class CardCourse extends StatelessWidget {
  final Area area;

  CardCourse({this.area});

  @override
  Widget build(BuildContext context) {
    final middleSection = Expanded(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              area.area,
              style: Style.commonTextStyle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );

    final cardAreaContent = Container(
        margin: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[middleSection],
        ));

    final courseAreaCard = ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            child: cardAreaContent,
            height: 80.0,
            width: (MediaQuery.of(context).size.width / 2) - 10,
            decoration: BoxDecoration(
              color: Colors.grey[300].withOpacity(0.7),
            ),
          ),
        ));

    return GestureDetector(
        onTap: () {
          Navigator.of(context).push(
                  PageRouteBuilder(
                    pageBuilder: (_, __, ___) => AreasList(
                        cursos: area.cursos,
                        area: area,
                      ),
                    transitionsBuilder:
                        (context, animation, secondaryAnimation, child) =>
                            FadeTransition(opacity: animation, child: child),
                  ),
                );
        },
        child: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 5.0,
            ),
            child: courseAreaCard));
  }
}
