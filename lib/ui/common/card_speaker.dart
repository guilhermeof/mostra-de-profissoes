import 'package:app_event/ui/detail/detail_speaker.dart';
import 'package:app_event/ui/styles/text_style.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CardSpeaker extends StatelessWidget {
  final DocumentSnapshot speaker;

  CardSpeaker(this.speaker);

  @override
  Widget build(BuildContext context) {
    final leftSection = Container(
      child: Hero(
          tag: "speaker-hero-${speaker['spk_id']}",
          child: ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Image(
              fit: BoxFit.cover,
              image: NetworkImage(speaker['spk_photo']),
              height: 60.0,
              width: 60.0,
            ),
          )),
    );

    final middleSection = Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              speaker['spk_name'],
              style: Style.commonTextStyle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              speaker['spk_local'],
              style: Style.smallTextSpeakerListCard,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );

    final cardAreaContent = Container(
        margin: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[leftSection, middleSection],
        ));

    final speakerCard = Container(
      child: cardAreaContent,
      height: 80.0,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: Offset(0.0, 5.0),
          ),
        ],
      ),
    );

    return GestureDetector(
        onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            DetailSpeaker(speaker)));
              },
        child: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 5.0,
            ),
            child: speakerCard));
  }
}
