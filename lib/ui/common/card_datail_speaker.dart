import 'package:app_event/ui/detail/detail_speaker.dart';
import 'package:app_event/ui/styles/text_style.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:app_event/ui/common/separator.dart';

class CardDetailSpeaker extends StatelessWidget {
  final DocumentSnapshot speaker;
  final bool horizontal;

  CardDetailSpeaker(this.speaker, {this.horizontal = true});

  CardDetailSpeaker.vertical(this.speaker) : horizontal = false;

  @override
  Widget build(BuildContext context) {
    final speakerThumbnail = Container(
      margin: EdgeInsets.symmetric(vertical: 25.0, horizontal: 15.0),
      alignment:
          horizontal ? FractionalOffset.centerLeft : FractionalOffset.center,
      child: Hero(
        tag: "speaker-hero-${speaker['spk_id']}",
        child: ClipRRect(
          borderRadius: BorderRadius.circular(40),
          child: Image(
          fit: BoxFit.cover,
          image: NetworkImage(speaker['spk_photo']),
          height: 80.0,
          width: 80.0,
        ),
        )
      ),
    );

    Widget _speakerValue({String value, IconData icon}) {
      return Container(
        child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Icon(icon, size: 16.0),
          SizedBox(width: 8.0),
          Text(
            value,
            style: Style.smallTextStyle,
          ),
        ]),
      );
    }

    final speakercardAreaContent = Container(
      margin: EdgeInsets.fromLTRB(
          horizontal ? 25.0 : 16.0, horizontal ? 16.0 : 42.0, 16.0, 16.0),
      // constraints: BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment:
            horizontal ? CrossAxisAlignment.start : CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 5.0),
          Text(speaker['spk_name'], style: Style.commonTextStyle, maxLines: 2, overflow: TextOverflow.ellipsis,),
          Separator(),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  flex: horizontal ? 1 : 0,
                  child: _speakerValue(
                    icon: Icons.timer,
                    value: speaker['spk_local'],
                  )),
              SizedBox(
                width: 15.0,
              ),
              Expanded(
                  flex: horizontal ? 1 : 0,
                  child: _speakerValue(
                    icon: Icons.timer,
                    value: speaker['spk_local'],
                  ))
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _speakerValue(
                icon: Icons.pin_drop,
                value: speaker['spk_local'],
              )
            ],
          )
        ],
      ),
    );

    final speakerCard = Container(
      child: speakercardAreaContent,
      alignment: Alignment.center,
      height: horizontal ? 124.0 : 154.0,
      margin:
          horizontal ? EdgeInsets.only(left: 0.0) : EdgeInsets.only(top: 72.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            offset: Offset(0.0, 5.0),
          ),
        ],
      ),
    );

    return GestureDetector(
        onTap: horizontal
            ? () {
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => DetailSpeaker(speaker)
              ));
            }
            : null,
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 15.0,
          ),
          child: Stack(
            children: <Widget>[
              speakerCard,
              speakerThumbnail,
            ],
          ),
        ));
  }
}
